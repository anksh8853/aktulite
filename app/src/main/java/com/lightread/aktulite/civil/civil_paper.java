package com.lightread.aktulite.civil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.lightread.aktulite.Expandable_subject.First_yr_expandable;
import com.lightread.aktulite.ExpandablelistAdapter;
import com.lightread.aktulite.R;
import com.lightread.aktulite.civil.concrete.concrete1;
import com.lightread.aktulite.civil.concrete.concrete2;
import com.lightread.aktulite.civil.designofstri.dos1;
import com.lightread.aktulite.civil.designofstri.dos2;
import com.lightread.aktulite.civil.dos2.dosii1;
import com.lightread.aktulite.civil.envirengg.envirengg1;
import com.lightread.aktulite.civil.fluidmech.fluidmech1;
import com.lightread.aktulite.civil.fluidmech.fluidmech2;
import com.lightread.aktulite.civil.fluidmech.fluidmech3;
import com.lightread.aktulite.civil.fluidmech.fluidmech4;
import com.lightread.aktulite.civil.geologynsoil.gns1;
import com.lightread.aktulite.civil.geothenical.geo1;
import com.lightread.aktulite.civil.geothenical.geo2;
import com.lightread.aktulite.civil.geothenical.geo3;
import com.lightread.aktulite.civil.geothenical.geo4;
import com.lightread.aktulite.civil.quantity.q1;
import com.lightread.aktulite.civil.quantity.q2;
import com.lightread.aktulite.civil.railway.r1;
import com.lightread.aktulite.civil.surveying.survey1;
import com.lightread.aktulite.civil.transportation.trans1;
import com.lightread.aktulite.civil.water.w1;
import com.lightread.aktulite.civil.water.w2;
import com.lightread.aktulite.civil.water.w3;
import com.lightread.aktulite.civil.water.w4;
import com.lightread.aktulite.cs.cyber.cyber1;
import com.lightread.aktulite.cs.cyber.cyber2;
import com.lightread.aktulite.cs.eco.eco1;
import com.lightread.aktulite.cs.eco.eco2;
import com.lightread.aktulite.cs.human.human1;
import com.lightread.aktulite.cs.human.human2;
import com.lightread.aktulite.cs.human.human3;
import com.lightread.aktulite.cs.im.im1;
import com.lightread.aktulite.cs.im.im2;
import com.lightread.aktulite.cs.im.im3;
import com.lightread.aktulite.cs.math4.math41;
import com.lightread.aktulite.cs.socio.socio1;
import com.lightread.aktulite.cs.socio.socio2;
import com.lightread.aktulite.cs.tc.tc;
import com.lightread.aktulite.mech.enggmech.em;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class civil_paper extends AppCompatActivity {
    private ExpandableListView listView;
    private ExpandablelistAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil_paper);



        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        listView=findViewById(R.id.exp);
        initData();
        listAdapter=new ExpandablelistAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);

    }

    private void initData() {

        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();

        //-----------------------------------------------------
        listDataHeader.add("Mathematics IV");
        listDataHeader.add("Enggineering Mechanics ");
        listDataHeader.add("Universal Human Value & Professional Ethics ");
        listDataHeader.add("Surveying and Geomatics");
        listDataHeader.add("Fluid Mechanics");
        listDataHeader.add("Enggineering Science Course ");
        listDataHeader.add("Technical Communication");
        listDataHeader.add("Materials, Testing & Construction Practices");
        listDataHeader.add("Introduction to Solid Mechanics ");
        listDataHeader.add("Hydraulic Engineering and Machines ");


        //------------------------------------------------------
        listDataHeader.add("Managerial Economics");
        listDataHeader.add("Sociology");
        listDataHeader.add("Geothenical Engineering");
        listDataHeader.add("Design Of Structure I");
        listDataHeader.add("Quantity Estimation And Managment");
        listDataHeader.add("Concrete Technology");
        listDataHeader.add("Industrial Managment");
        listDataHeader.add("Cyber Security");
        listDataHeader.add("Design Of Structure II");
        listDataHeader.add("Environmental Engineering");
        listDataHeader.add("Transportation Engineering");
        listDataHeader.add("Integrated Waste Managment For A Smart City");

        //------------------------------------------------------

        listDataHeader.add("Geology and Soil Mechanics");
        listDataHeader.add("Railways, Airport & Water Ways");
        listDataHeader.add("Design of Structure III");
        listDataHeader.add("Water Resources");
        listDataHeader.add("");













        List<String> l1=new ArrayList<>();
        l1.add("1.   Question Paper (2019-2020)");





        List<String> l2=new ArrayList<>();
        l2.add("1.   Question Paper (2019-2020)");


        List<String> l3=new ArrayList<>();

        l3.add("1.   Question Paper (2018-2019)");
        l3.add("2.   Question Paper (2017-2018)");
        l3.add("3.   Question Paper (2016-2017)");



        List<String> l4=new ArrayList<>();
        l4.add("1.   Question Paper (2019-2020)");



        List<String> l5=new ArrayList<>();
        l5.add("1.   Question Paper (2019-2020)");
        l5.add("2.   Question Paper (2018-2019)");
        l5.add("3.   Question Paper (2017-2018)");
        l5.add("4.   Question Paper (2016-2017)");


        List<String> l6=new ArrayList<>();
        l6.add("Previous year Paper Not Found");


        List<String> l7=new ArrayList<>();
        l7.add("1.   Question Paper (2019-2020");



        List<String> l8=new ArrayList<>();
        l8.add("Previous year Paper Not Found");

        List<String> l9=new ArrayList<>();
        l9.add("Previous year Paper Not Found");

        List<String> l10=new ArrayList<>();
        l10.add("Previous year Paper Not Found");

//----------------------------------------------------------------------
        List<String> l11=new ArrayList<>();
        l11.add("1.   Question Paper (2019-2020");
        l11.add("2.   Question Paper (2018-2019");

        List<String> l12=new ArrayList<>();
        l12.add("1.   Question Paper (2019-2020)");
        l12.add("2.   Question Paper (2018-2019)");


        List<String> l13=new ArrayList<>();
        l13.add("1.   Question Paper (2019-2020)");
        l13.add("2.   Question Paper (2018-2019)");
        l13.add("3.   Question Paper (2017-2018)");
        l13.add("4.   Question Paper (2016-2017)");

        List<String> l14=new ArrayList<>();
        l14.add("1.   Question Paper (2019-2020)");
        l14.add("2.   Question Paper (2018-2019)");


        List<String> l15=new ArrayList<>();
        l15.add("1.   Question Paper (2019-2020)");
        l15.add("2.   Question Paper (2018-2019)");



        List<String> l16=new ArrayList<>();
        l16.add("1.   Question Paper (2019-2020)");
        l16.add("2.   Question Paper (2018-2019)");


        List<String> l17=new ArrayList<>();

        l17.add("1.   Question Paper (2018-2019)");
        l17.add("2.   Question Paper (2017-2018)");
        l17.add("3.   Question Paper (2016-2017)");


        List<String> l18=new ArrayList<>();
        l18.add("1.   Question Paper (2019-2020)");
        l18.add("2.   Question Paper (2018-2019)");


        List<String> l19=new ArrayList<>();
        l19.add("1.   Question Paper (2018-2019)");



        List<String> l20=new ArrayList<>();

        l20.add("1.   Question Paper (2018-2019)");


        List<String> l21=new ArrayList<>();

        l21.add("1.   Question Paper (2018-2019)");


        List<String> l22=new ArrayList<>();
        l22.add("Previous year Paper Not Found");




        //------------------------------------------------
        List<String> l23=new ArrayList<>();
        l23.add("1.   Question Paper (2019-2020)");




        List<String> l24=new ArrayList<>();
        l24.add("1.   Question Paper (2019-2020)");


        List<String> l25=new ArrayList<>();
        l25.add("Previous year Paper Not Found");


        List<String> l26=new ArrayList<>();
        l26.add("1.   Question Paper (2019-2020)");
        l26.add("2.   Question Paper (2018-2019)");
        l26.add("3.   Question Paper (2017-2018)");
        l26.add("4.   Question Paper (2016-2017)");

        List<String> l27=new ArrayList<>();


        listHash.put(listDataHeader.get(0),l1);
        listHash.put(listDataHeader.get(1),l2);
        listHash.put(listDataHeader.get(2),l3);
        listHash.put(listDataHeader.get(3),l4);
        listHash.put(listDataHeader.get(4),l5);
        listHash.put(listDataHeader.get(5),l6);
        listHash.put(listDataHeader.get(6),l7);
        listHash.put(listDataHeader.get(7),l8);
        listHash.put(listDataHeader.get(8),l9);
        listHash.put(listDataHeader.get(9),l10);
        listHash.put(listDataHeader.get(10),l11);
        listHash.put(listDataHeader.get(11),l12);
        listHash.put(listDataHeader.get(12),l13);
        listHash.put(listDataHeader.get(13),l14);
        listHash.put(listDataHeader.get(14),l15);
        listHash.put(listDataHeader.get(15),l16);
        listHash.put(listDataHeader.get(16),l17);
        listHash.put(listDataHeader.get(17),l18);
        listHash.put(listDataHeader.get(18),l19);
        listHash.put(listDataHeader.get(19),l20);
        listHash.put(listDataHeader.get(20),l21);
        listHash.put(listDataHeader.get(21),l22);
        listHash.put(listDataHeader.get(22),l23);
        listHash.put(listDataHeader.get(23),l24);
        listHash.put(listDataHeader.get(24),l25);
        listHash.put(listDataHeader.get(25),l26);
        listHash.put(listDataHeader.get(26),l27);





        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                if (groupPosition==0 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, math41.class);
                    startActivity(intent);


                }
 //----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==1 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, em.class);
                    startActivity(intent);


                }

 //---------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==2 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, human1.class);
                    startActivity(intent);


                }
                if (groupPosition==2 && childPosition==1) {
                    Intent intent=new Intent(civil_paper.this, human2.class);
                    startActivity(intent);


                }
                if (groupPosition==2 && childPosition==2) {
                    Intent intent=new Intent(civil_paper.this, human3.class);
                    startActivity(intent);


                }

  //--------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==3 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, survey1.class);
                    startActivity(intent);


                }
//---------------------------------------------------------------------------------------------------------------



                if (groupPosition==4 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, fluidmech1.class);
                    startActivity(intent);


                }
                if (groupPosition==4 && childPosition==1) {
                    Intent intent=new Intent(civil_paper.this, fluidmech2.class);
                    startActivity(intent);


                }
                if (groupPosition==4 && childPosition==2) {
                    Intent intent = new Intent(civil_paper.this, fluidmech3.class);
                    startActivity(intent);


                }
                if (groupPosition==4 && childPosition==3) {
                    Intent intent=new Intent(civil_paper.this, fluidmech4.class);
                    startActivity(intent);


                }
//----------------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==5 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }
//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==6 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, tc.class);
                    startActivity(intent);

                }
//----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==7 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }
//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==8 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }
//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==9 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }
//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==10 && childPosition==0) {
                    Intent intent=new Intent(civil_paper.this, eco1.class);
                    startActivity(intent);


                }
                if (groupPosition==10 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, eco2.class);
                    startActivity(intent);
                }
//------------------------------------------------------------------------------------------------------------------------


                if (groupPosition==11 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, socio1.class);
                    startActivity(intent);
                }

                    if (groupPosition==11 && childPosition==1) {
                        Intent intent = new Intent(civil_paper.this, socio2.class);
                        startActivity(intent);

                    }

//------------------------------------------------------------------------------------------------------------



                if (groupPosition==12 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, geo1.class);
                    startActivity(intent);
                }

                if (groupPosition==12 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, geo2.class);
                    startActivity(intent);

                }
                if (groupPosition==12 && childPosition==2) {
                    Intent intent = new Intent(civil_paper.this, geo3.class);
                    startActivity(intent);
                }

                if (groupPosition==12 && childPosition==3) {
                    Intent intent = new Intent(civil_paper.this, geo4.class);
                    startActivity(intent);

                }

//-------------------------------------------------------------------------------------------------------------------

                if (groupPosition==13 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, dos1.class);
                    startActivity(intent);
                }

                if (groupPosition==13 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, dos2.class);
                    startActivity(intent);

                }

//-----------------------------------------------------------------------------------------------------------------
                if (groupPosition==14 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, q1.class);
                    startActivity(intent);
                }

                if (groupPosition==14 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, q2.class);
                    startActivity(intent);

                }
//---------------------------------------------------------------------------------------------------------------------

                if (groupPosition==15 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, concrete1.class);
                    startActivity(intent);
                }

                if (groupPosition==15 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, concrete2.class);
                    startActivity(intent);

                }

//-------------------------------------------------------------------------------------------------


                if (groupPosition==16 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, im1.class);
                    startActivity(intent);
                }

                if (groupPosition==16 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, im2.class);
                    startActivity(intent);

                }

                if (groupPosition==16 && childPosition==2) {
                    Intent intent = new Intent(civil_paper.this, im3.class);
                    startActivity(intent);

                }

//-------------------------------------------------------------------------------------------------
                if (groupPosition==17 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, cyber1.class);
                    startActivity(intent);
                }

                if (groupPosition==17 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, cyber2.class);
                    startActivity(intent);

                }
//---------------------------------------------------------------------------------------------------------------

                if (groupPosition==18 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, dosii1.class);
                    startActivity(intent);
                }
//-----------------------------------------------------------------------------------------------------------

                if (groupPosition==19 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, envirengg1.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------
                if (groupPosition==20 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, trans1.class);
                    startActivity(intent);
                }
//---------------------------------------------------------------------------------------------------------------



                if (groupPosition==21 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }
//----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==22 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, gns1.class);
                    startActivity(intent);
                }

//---------------------------------------------------------------------------------------------------------
                if (groupPosition==23 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, r1.class);
                    startActivity(intent);
                }
//----------------------------------------------------------------------------------------------------------------


                if (groupPosition==24 && childPosition==0) {
                    Toast.makeText(civil_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();

                }

//----------------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==25 && childPosition==0) {
                    Intent intent = new Intent(civil_paper.this, w1.class);
                    startActivity(intent);
                }
                if (groupPosition==25 && childPosition==1) {
                    Intent intent = new Intent(civil_paper.this, w2.class);
                    startActivity(intent);
                }
                if (groupPosition==25 && childPosition==2) {
                    Intent intent = new Intent(civil_paper.this, w3.class);
                    startActivity(intent);
                }
                if (groupPosition==25 && childPosition==3) {
                    Intent intent = new Intent(civil_paper.this, w4.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------------------------------




                return true;
            }
        });

    }


}
