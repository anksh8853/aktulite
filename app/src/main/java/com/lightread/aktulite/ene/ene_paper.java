package com.lightread.aktulite.ene;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.lightread.aktulite.ExpandablelistAdapter;
import com.lightread.aktulite.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ene_paper extends AppCompatActivity {

    private ExpandableListView listView;
    private ExpandablelistAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ene_paper);


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);




        listView=findViewById(R.id.exp);
        initData();
        listAdapter=new ExpandablelistAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);

    }

    private void initData() {

        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();

        listDataHeader.add("Laser System and Application");
        listDataHeader.add("Environment &  Ecology");
        listDataHeader.add("Analog & Digital Electronics");
        listDataHeader.add("Electrical & Electronics Engineering Materials");
        listDataHeader.add("Electrical Measurements &  Instrumentation");
        listDataHeader.add("Basic Signals & Systems");
        listDataHeader.add("Mathematics IV");
        listDataHeader.add("Universal Human Values & Professional Ethics");
        listDataHeader.add("Electromagnetic Field Theory");
        listDataHeader.add("Power Plant Engineering");
        listDataHeader.add("Electrical Machines I");
        listDataHeader.add("Network Analysis and Synthesis");
        //------------------------------------------------------

        listDataHeader.add("Managerial Economics");
        listDataHeader.add("Sociology");
        listDataHeader.add("Electrical Machine II");
        listDataHeader.add("Power Transmission & Distribution");
        listDataHeader.add("Control System");
        listDataHeader.add("Principles of Communication");

        listDataHeader.add("Industrial Managment");
        listDataHeader.add("Cyber Security");
        listDataHeader.add("Power Electronics");
        listDataHeader.add("Microprocessor");
        listDataHeader.add("Power System Analysis");

        //------------------------------------------------------

        listDataHeader.add("Utilization of Electrical Energy & Electric Traction");
        listDataHeader.add("Energy Efficiency & Conservation");
        listDataHeader.add("Communication System");

        listDataHeader.add("Power System Protection");













        List<String> l1=new ArrayList<>();
        l1.add("1.   Question Paper (2019-2020)");





        List<String> l2=new ArrayList<>();
        l2.add("1.   Question Paper (2019-2020)");


        List<String> l3=new ArrayList<>();
        l3.add("1.   Question Paper (2019-2020)");
        l3.add("2.   Question Paper (2018-2019)");
        l3.add("3.   Question Paper (2017-2018)");
        l3.add("4.   Question Paper (2016-2017)");



        List<String> l4=new ArrayList<>();
        l4.add("1.   Question Paper (2019-2020)");
        l4.add("2.   Question Paper (2018-2019)");
        l4.add("3.   Question Paper (2017-2018)");
        l4.add("4.   Question Paper (2016-2017)");



        List<String> l5=new ArrayList<>();
        l5.add("1.   Question Paper (2019-2020)");
        l5.add("2.   Question Paper (2018-2019)");
        l5.add("3.   Question Paper (2017-2018)");
        l5.add("4.   Question Paper (2016-2017)");


        List<String> l6=new ArrayList<>();
        l6.add("1.   Question Paper (2019-2020)");


        List<String> l7=new ArrayList<>();
        l7.add("1.   Question Paper (2019-2020");
        l7.add("2.   Question Paper (2018-2019");
        l7.add("3.   Question Paper (2017-2018");
        l7.add("4.   Question Paper (2016-2017");


        List<String> l8=new ArrayList<>();
        l8.add("1.   Question Paper (2019-2020");
        l8.add("2.   Question Paper (2018-2019");
        l8.add("3.   Question Paper (2017-2018");
        l8.add("4.   Question Paper (2016-2017");


        List<String> l9=new ArrayList<>();
        l9.add("1.   Question Paper (2019-2020");
        l9.add("2.   Question Paper (2018-2019");
        l9.add("3.   Question Paper (2017-2018");
        l9.add("4.   Question Paper (2016-2017");


        List<String> l10=new ArrayList<>();
        l10.add("1.   Question Paper (2019-2020");
        l10.add("2.   Question Paper (2018-2019");
        l10.add("3.   Question Paper (2017-2018");
        l10.add("4.   Question Paper (2016-2017");


        List<String> l11=new ArrayList<>();
        l11.add("1.   Question Paper (2019-2020");

        List<String> l12=new ArrayList<>();
        l12.add("1.   Question Paper (2019-2020)");
        l12.add("2.   Question Paper (2018-2019)");
        l12.add("3.   Question Paper (2017-2018)");
        l12.add("4.   Question Paper (2016-2017)");
//----------------------------------------------------------------------


        List<String> l13=new ArrayList<>();
        l13.add("1.   Question Paper (2019-2020)");
        l13.add("2.   Question Paper (2018-2019)");
        l13.add("3.   Question Paper (2017-2018)");
        l13.add("4.   Question Paper (2016-2017)");

        List<String> l14=new ArrayList<>();
        l14.add("1.   Question Paper (2019-2020)");
        l14.add("2.   Question Paper (2018-2019)");
        l14.add("3.   Question Paper (2017-2018)");
        l14.add("4.   Question Paper (2016-2017)");

        List<String> l15=new ArrayList<>();
        l15.add("1.   Question Paper (2019-2020)");
        l15.add("2.   Question Paper (2018-2019)");
        l15.add("3.   Question Paper (2017-2018)");
        l15.add("4.   Question Paper (2016-2017)");


        List<String> l16=new ArrayList<>();
        l16.add("1.   Question Paper (2019-2020)");
        l16.add("2.   Question Paper (2018-2019)");
        l16.add("3.   Question Paper (2017-2018)");
        l16.add("4.   Question Paper (2016-2017)");

        List<String> l17=new ArrayList<>();
        l17.add("1.   Question Paper (2019-2020)");
        l17.add("2.   Question Paper (2018-2019)");
        l17.add("3.   Question Paper (2017-2018)");
        l17.add("4.   Question Paper (2016-2017)");

        List<String> l18=new ArrayList<>();
        l18.add("1.   Question Paper (2019-2020)");
        l18.add("2.   Question Paper (2018-2019)");
        l18.add("3.   Question Paper (2017-2018)");
        l18.add("4.   Question Paper (2016-2017)");

        List<String> l19=new ArrayList<>();
        l19.add("1.   Question Paper (2019-2020)");
        l19.add("2.   Question Paper (2018-2019)");
        l19.add("3.   Question Paper (2017-2018)");
        l19.add("4.   Question Paper (2016-2017)");


        List<String> l20=new ArrayList<>();
        l20.add("1.   Question Paper (2019-2020)");
        l20.add("2.   Question Paper (2018-2019)");
        l20.add("3.   Question Paper (2017-2018)");
        l20.add("4.   Question Paper (2016-2017)");

        List<String> l21=new ArrayList<>();
        l21.add("1.   Question Paper (2019-2020)");
        l21.add("2.   Question Paper (2018-2019)");
        l21.add("3.   Question Paper (2017-2018)");
        l21.add("4.   Question Paper (2016-2017)");

        List<String> l22=new ArrayList<>();
        l22.add("1.   Question Paper (2019-2020)");
        l22.add("2.   Question Paper (2018-2019)");
        l22.add("3.   Question Paper (2017-2018)");
        l22.add("4.   Question Paper (2016-2017)");


        List<String> l23=new ArrayList<>();
        l23.add("1.   Question Paper (2019-2020)");
        l23.add("2.   Question Paper (2018-2019)");
        l23.add("3.   Question Paper (2017-2018)");
        l23.add("4.   Question Paper (2016-2017)");


        //------------------------------------------------




        List<String> l24=new ArrayList<>();
        l24.add("1.   Question Paper (2019-2020)");
        l24.add("2.   Question Paper (2018-2019)");
        l24.add("3.   Question Paper (2017-2018)");
        l24.add("4.   Question Paper (2016-2017)");

        List<String> l25=new ArrayList<>();
        l25.add("1.   Question Paper (2019-2020)");
        l25.add("2.   Question Paper (2018-2019)");
        l25.add("3.   Question Paper (2017-2018)");
        l25.add("4.   Question Paper (2016-2017)");

        List<String> l26=new ArrayList<>();
        l26.add("1.   Question Paper (2019-2020)");
        l26.add("2.   Question Paper (2018-2019)");
        l26.add("3.   Question Paper (2017-2018)");
        l26.add("4.   Question Paper (2016-2017)");

        List<String> l27=new ArrayList<>();
        l27.add("1.   Question Paper (2019-2020)");
        l27.add("2.   Question Paper (2018-2019)");
        l27.add("3.   Question Paper (2017-2018)");
        l27.add("4.   Question Paper (2016-2017)");






        listHash.put(listDataHeader.get(0),l1);
        listHash.put(listDataHeader.get(1),l2);
        listHash.put(listDataHeader.get(2),l3);
        listHash.put(listDataHeader.get(3),l4);
        listHash.put(listDataHeader.get(4),l5);
        listHash.put(listDataHeader.get(5),l6);
        listHash.put(listDataHeader.get(6),l7);
        listHash.put(listDataHeader.get(7),l8);
        listHash.put(listDataHeader.get(8),l9);
        listHash.put(listDataHeader.get(9),l10);
        listHash.put(listDataHeader.get(10),l11);
        listHash.put(listDataHeader.get(11),l12);
        listHash.put(listDataHeader.get(12),l13);
        listHash.put(listDataHeader.get(13),l14);
        listHash.put(listDataHeader.get(14),l15);
        listHash.put(listDataHeader.get(15),l16);
        listHash.put(listDataHeader.get(16),l17);
        listHash.put(listDataHeader.get(17),l18);
        listHash.put(listDataHeader.get(18),l19);
        listHash.put(listDataHeader.get(19),l20);
        listHash.put(listDataHeader.get(20),l21);
        listHash.put(listDataHeader.get(21),l22);
        listHash.put(listDataHeader.get(22),l23);
        listHash.put(listDataHeader.get(23),l24);
        listHash.put(listDataHeader.get(24),l25);
        listHash.put(listDataHeader.get(25),l26);
        listHash.put(listDataHeader.get(26),l27);


        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                if (groupPosition==0 && childPosition==0) {



                }
                if (groupPosition==0 && childPosition==1)
                {
                    Toast.makeText(ene_paper.this, "sharma ", Toast.LENGTH_SHORT).show();
                }

                if (groupPosition==0 && childPosition==2)
                {

                }


                return true;
            }
        });

    }


}
