package com.lightread.aktulite;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lightread.aktulite.Expandable_subject.First_yr_expandable;
import com.lightread.aktulite.ene.ene_paper;
import com.lightread.aktulite.civil.civil_paper;
import com.lightread.aktulite.cs.cs_paper;
import com.lightread.aktulite.ec.ec_paper;
import com.lightread.aktulite.ele.ele_paper;
import com.lightread.aktulite.mech.mech_paper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements PersonAdapter.ItemClicked {
    RecyclerView recyclerView;
    RecyclerView.Adapter myadapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Person> people;
    TextView maq;
ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkConnection();





        recyclerView = findViewById(R.id.list);
        progressBar=findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);

        maq=findViewById(R.id.maq);
        maq.setSelected(true);


       // layoutManager=new LinearLayoutManager(this);



        people = new ArrayList<Person>();
        people.add(new Person("First Year", R.drawable.first));
        people.add(new Person("CS/IT Engineer", R.drawable.cse));
        people.add(new Person("Mechanical Engineer", R.drawable.mech));
        people.add(new Person("Civil Engineer", R.drawable.civil));
        people.add(new Person("Electrical Engineer", R.drawable.bulb));
        people.add(new Person("E&C  Engineer", R.drawable.ec));
        people.add(new Person("E&E  Engineer", R.drawable.ene));
        people.add(new Person("AKTU Services", R.drawable.aktu));

        layoutManager = new GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        myadapter = new PersonAdapter(people,this);

        recyclerView.setAdapter(myadapter);


    }

    @Override
    public void onItemClicked(int index) {

        Toast.makeText(this, "Loading...", Toast.LENGTH_SHORT).show();


        switch (index) {
            case 0:

                Intent intent = new Intent(this, First_yr_expandable.class);

                this.startActivity(intent);
                this.overridePendingTransition(0, 0);

                break;

            case 1:
                Intent   intent1 = new Intent(this, cs_paper.class);
                this.startActivity(intent1);
                this.overridePendingTransition(0, 0);

                break;

            case 2:
                Intent  intent2 = new Intent(this, mech_paper.class);
                this.startActivity(intent2);
                this.overridePendingTransition(0, 0);

                break;

            case 3:
                Intent intent3 = new Intent(this, civil_paper.class);
                this.startActivity(intent3);
                this.overridePendingTransition(0, 0);

                break;

            case 4:
                Toast.makeText(this, "Coming Soon!", Toast.LENGTH_SHORT).show();
                break;

            case 5:
                Toast.makeText(this, "Coming Soon!", Toast.LENGTH_SHORT).show();
                break;

            case 6:
                Toast.makeText(this, "Coming Soon!", Toast.LENGTH_SHORT).show();
                break;

            case 7:
                intent = new Intent(this, AktuService.class);
                this.startActivity(intent);
                break;


        }
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)

                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }



    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }
}







