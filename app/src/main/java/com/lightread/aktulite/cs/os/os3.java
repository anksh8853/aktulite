package com.lightread.aktulite.cs.os;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lightread.aktulite.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.lightread.aktulite.cs.micro.micro1;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class os3 extends AppCompatActivity {

    PDFView pdfView;
    TextView textView;
    ProgressBar progressBar;
    private FirebaseDatabase database= FirebaseDatabase.getInstance();
    DatabaseReference mref=database.getReference("os3");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_os3);

        pdfView=findViewById(R.id.pdfview);
        textView=findViewById(R.id.text1);
        progressBar=findViewById(R.id.progress);
        Toast.makeText(os3.this, "Loading...", Toast.LENGTH_LONG).show();
        mref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String value=dataSnapshot.getValue(String.class);
                textView.setText(value);
                String url=textView.getText().toString();
                new RetrivePdfStream().execute(url);
            }@Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(os3.this, "fail to load", Toast.LENGTH_SHORT).show(); }}); }
    class RetrivePdfStream extends AsyncTask<String,Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {

                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }

            } catch (IOException e) {
                return null;
            }
            return inputStream; }
        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream).load();
            progressBar.setVisibility(ProgressBar.GONE);
        }}}



