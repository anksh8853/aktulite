package com.lightread.aktulite.cs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.google.android.gms.ads.AdSize;
import com.lightread.aktulite.ExpandablelistAdapter;
import com.lightread.aktulite.R;

import com.lightread.aktulite.cs.artificialinteligence.ai1;
import com.lightread.aktulite.cs.artificialinteligence.ai2;
import com.lightread.aktulite.cs.artificialinteligence.ai3;
import com.lightread.aktulite.cs.artificialinteligence.ai4;
import com.lightread.aktulite.cs.auto.auto1;
import com.lightread.aktulite.cs.auto.auto2;
import com.lightread.aktulite.cs.auto.auto3;
import com.lightread.aktulite.cs.cloudcomputing.cc;
import com.lightread.aktulite.cs.coa.coa1;
import com.lightread.aktulite.cs.coa.coa2;
import com.lightread.aktulite.cs.coa.coa3;
import com.lightread.aktulite.cs.compiler.cd1;
import com.lightread.aktulite.cs.compiler.cd2;
import com.lightread.aktulite.cs.compiler.cd3;
import com.lightread.aktulite.cs.csgraphics.cg1;
import com.lightread.aktulite.cs.csnetwork.cn1;
import com.lightread.aktulite.cs.csnetwork.cn2;
import com.lightread.aktulite.cs.csnetwork.cn3;
import com.lightread.aktulite.cs.cyber.cyber1;
import com.lightread.aktulite.cs.cyber.cyber2;
import com.lightread.aktulite.cs.daa.daa1;
import com.lightread.aktulite.cs.daa.daa2;
import com.lightread.aktulite.cs.daa.daa3;
import com.lightread.aktulite.cs.daa.daa4;
import com.lightread.aktulite.cs.dataware.dw1;
import com.lightread.aktulite.cs.dataware.dw2;
import com.lightread.aktulite.cs.dataware.dw3;
import com.lightread.aktulite.cs.dbms.db1;
import com.lightread.aktulite.cs.dbms.db2;
import com.lightread.aktulite.cs.dbms.db3;
import com.lightread.aktulite.cs.dbms.db4;
import com.lightread.aktulite.cs.distributedsystem.distri1;
import com.lightread.aktulite.cs.distributedsystem.distri2;
import com.lightread.aktulite.cs.distributedsystem.distri3;
import com.lightread.aktulite.cs.distributedsystem.distri4;
import com.lightread.aktulite.cs.ds.ds1;
import com.lightread.aktulite.cs.ds.ds2;
import com.lightread.aktulite.cs.ds.ds3;
import com.lightread.aktulite.cs.ds.ds4;
import com.lightread.aktulite.cs.dstl.dstl1;
import com.lightread.aktulite.cs.dstl.dstl2;
import com.lightread.aktulite.cs.dstl.dstl3;
import com.lightread.aktulite.cs.eco.eco1;
import com.lightread.aktulite.cs.eco.eco2;
import com.lightread.aktulite.cs.human.human1;
import com.lightread.aktulite.cs.human.human2;
import com.lightread.aktulite.cs.human.human3;
import com.lightread.aktulite.cs.im.im1;
import com.lightread.aktulite.cs.im.im2;
import com.lightread.aktulite.cs.im.im3;
import com.lightread.aktulite.cs.math4.math41;
import com.lightread.aktulite.cs.micro.micro1;
import com.lightread.aktulite.cs.micro.micro2;
import com.lightread.aktulite.cs.micro.micro3;
import com.lightread.aktulite.cs.os.os1;
import com.lightread.aktulite.cs.os.os2;
import com.lightread.aktulite.cs.os.os3;
import com.lightread.aktulite.cs.ppl.pp1;
import com.lightread.aktulite.cs.ppl.pp2;
import com.lightread.aktulite.cs.ppl.ppl3;
import com.lightread.aktulite.cs.ppl.ppl4;
import com.lightread.aktulite.cs.python.python;
import com.lightread.aktulite.cs.sensor.sensor;
import com.lightread.aktulite.cs.socio.socio1;
import com.lightread.aktulite.cs.socio.socio2;
import com.lightread.aktulite.cs.softcomputing.sc;
import com.lightread.aktulite.cs.tc.tc;
import com.lightread.aktulite.cs.web.wt1;
import com.lightread.aktulite.cs.web.wt2;
import com.lightread.aktulite.cs.web.wt3;
import com.lightread.aktulite.cs.web.wt4;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class cs_paper extends AppCompatActivity {

    private ExpandableListView listView;
    private ExpandablelistAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cs_paper);




        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);







        listView=findViewById(R.id.exp);
        initData();
        listAdapter=new ExpandablelistAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);

    }

    private void initData() {

        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();



         //-----------------------------------------------------
        listDataHeader.add("Sensor & Instrumentation");
        listDataHeader.add("Technical Communication");
        listDataHeader.add("Data Structure");
        listDataHeader.add("Computer Organization and Architecture");
        listDataHeader.add("Discrete Structures & Theory of Logic");
        listDataHeader.add("Mathematics-IV");
        listDataHeader.add("Theory of Automata and Formal Languages");
        listDataHeader.add("Universal Human Values & Professional Ethics");
        listDataHeader.add("Introduction to Microprocessor");
        listDataHeader.add("Operating Systems");
        listDataHeader.add("Python Programming");
         //------------------------------------------------------
        listDataHeader.add("Managerial Economics");
        listDataHeader.add("Cyber Security");
        listDataHeader.add("Database Management Systems");
        listDataHeader.add("Design and Analysis of Algorithm");
        listDataHeader.add("Principles of Programming Languages");
        listDataHeader.add("Web Technologies");
        listDataHeader.add("Industrial Managment");
        listDataHeader.add("Sociology");
        listDataHeader.add("Computer Networks");
        listDataHeader.add("Compiler Design");
        listDataHeader.add("Computer Graphics");
        listDataHeader.add("DataWareHousing & Data Mining");
        //------------------------------------------------------

        listDataHeader.add("Application of Soft Computing");
        listDataHeader.add("Cloud Computing");
        listDataHeader.add("Distributed System");
        listDataHeader.add("Artificial Intelligence");
        listDataHeader.add("");













        List<String> l1=new ArrayList<>();
        l1.add("1.   Question Paper (2019-2020)");





        List<String> l2=new ArrayList<>();
        l2.add("1.   Question Paper (2019-2020)");


        List<String> l3=new ArrayList<>();
        l3.add("1.   Question Paper (2019-2020)");
        l3.add("2.   Question Paper (2018-2019)");
        l3.add("3.   Question Paper (2017-2018)");
        l3.add("4.   Question Paper (2016-2017)");



        List<String> l4=new ArrayList<>();
        l4.add("1.   Question Paper (2019-2020)");
        l4.add("2.   Question Paper (2018-2019)");
        l4.add("3.   Question Paper (2017-2018)");



        List<String> l5=new ArrayList<>();
        l5.add("1.   Question Paper (2019-2020)");
        l5.add("2.   Question Paper (2018-2019)");
        l5.add("3.   Question Paper (2017-2018)");



        List<String> l6=new ArrayList<>();
        l6.add("1.   Question Paper (2019-2020)");


        List<String> l7=new ArrayList<>();

        l7.add("1.   Question Paper (2018-2019)");
        l7.add("2.   Question Paper (2017-2018)");
        l7.add("3.   Question Paper (2016-2017)");


        List<String> l8=new ArrayList<>();

        l8.add("1.   Question Paper (2018-2019)");
        l8.add("2.   Question Paper (2017-2018)");
        l8.add("3.   Question Paper (2016-2017)");


        List<String> l9=new ArrayList<>();

        l9.add("1.   Question Paper (2018-2019)");
        l9.add("2.   Question Paper (2017-2018)");
        l9.add("3.   Question Paper (2016-2017)");


        List<String> l10=new ArrayList<>();

        l10.add("1.   Question Paper (2018-2019)");
        l10.add("2.   Question Paper (2017-2018)");
        l10.add("3.   Question Paper (2016-2017)");


        List<String> l11=new ArrayList<>();
        l11.add("1.   Question Paper (2019-2020)-");
//----------------------------------------------------------------------
        List<String> l12=new ArrayList<>();
        l12.add("1.   Question Paper (2019-2020)");
        l12.add("2.   Question Paper (2018-2019)");


        List<String> l13=new ArrayList<>();
        l13.add("1.   Question Paper (2019-2020)");
        l13.add("2.   Question Paper (2018-2019)");


        List<String> l14=new ArrayList<>();
        l14.add("1.   Question Paper (2019-2020)");
        l14.add("2.   Question Paper (2018-2019)");
        l14.add("3.   Question Paper (2017-2018)");
        l14.add("4.   Question Paper (2016-2017)");

        List<String> l15=new ArrayList<>();
        l15.add("1.   Question Paper (2019-2020)");
        l15.add("2.   Question Paper (2018-2019)");
        l15.add("3.   Question Paper (2017-2018)");
        l15.add("4.   Question Paper (2016-2017)");


        List<String> l16=new ArrayList<>();
        l16.add("1.   Question Paper (2019-2020)");
        l16.add("2.   Question Paper (2018-2019)");
        l16.add("3.   Question Paper (2017-2018)");
        l16.add("4.   Question Paper (2016-2017)");

        List<String> l17=new ArrayList<>();
        l17.add("1.   Question Paper (2019-2020)");
        l17.add("2.   Question Paper (2018-2019)");
        l17.add("3.   Question Paper (2017-2018)");
        l17.add("4.   Question Paper (2016-2017)");

        List<String> l18=new ArrayList<>();

        l18.add("1.   Question Paper (2018-2019)");
        l18.add("2.   Question Paper (2017-2018)");
        l18.add("2.   Question Paper (2016-2017)");

        List<String> l19=new ArrayList<>();
        l19.add("1.   Question Paper (2019-2020)");
        l19.add("2.   Question Paper (2018-2019)");



        List<String> l20=new ArrayList<>();

        l20.add("1.   Question Paper (2018-2019)");
        l20.add("2.   Question Paper (2017-2018)");
        l20.add("3.   Question Paper (2016-2017)");

        List<String> l21=new ArrayList<>();

        l21.add("1.   Question Paper (2018-2019)");
        l21.add("2.   Question Paper (2017-2018)");
        l21.add("2.   Question Paper (2016-2017)");

        List<String> l22=new ArrayList<>();

        l22.add("1.   Question Paper (2018-2019)");



        List<String> l23=new ArrayList<>();

        l23.add("1.   Question Paper (2018-2019)");
        l23.add("2.   Question Paper (2017-2018)");
        l23.add("3.   Question Paper (2016-2017)");


        //------------------------------------------------




        List<String> l24=new ArrayList<>();
        l24.add("1.   Question Paper (2019-2020)");


        List<String> l25=new ArrayList<>();
        l25.add("1.   Question Paper (2019-2020)");


        List<String> l26=new ArrayList<>();
        l26.add("1.   Question Paper (2019-2020)");
        l26.add("2.   Question Paper (2018-2019)");
        l26.add("3.   Question Paper (2017-2018)");
        l26.add("4.   Question Paper (2016-2017)");

        List<String> l27=new ArrayList<>();
        l27.add("1.   Question Paper (2019-2020)");
        l27.add("2.   Question Paper (2018-2019)");
        l27.add("3.   Question Paper (2017-2018)");
        l27.add("4.   Question Paper (2016-2017)");

        List<String> l28=new ArrayList<>();




        listHash.put(listDataHeader.get(0),l1);
        listHash.put(listDataHeader.get(1),l2);
        listHash.put(listDataHeader.get(2),l3);
        listHash.put(listDataHeader.get(3),l4);
        listHash.put(listDataHeader.get(4),l5);
        listHash.put(listDataHeader.get(5),l6);
        listHash.put(listDataHeader.get(6),l7);
        listHash.put(listDataHeader.get(7),l8);
        listHash.put(listDataHeader.get(8),l9);
        listHash.put(listDataHeader.get(9),l10);
        listHash.put(listDataHeader.get(10),l11);
        listHash.put(listDataHeader.get(11),l12);
        listHash.put(listDataHeader.get(12),l13);
        listHash.put(listDataHeader.get(13),l14);
        listHash.put(listDataHeader.get(14),l15);
        listHash.put(listDataHeader.get(15),l16);
        listHash.put(listDataHeader.get(16),l17);
        listHash.put(listDataHeader.get(17),l18);
        listHash.put(listDataHeader.get(18),l19);
        listHash.put(listDataHeader.get(19),l20);
        listHash.put(listDataHeader.get(20),l21);
        listHash.put(listDataHeader.get(21),l22);
        listHash.put(listDataHeader.get(22),l23);
        listHash.put(listDataHeader.get(23),l24);
        listHash.put(listDataHeader.get(24),l25);
        listHash.put(listDataHeader.get(25),l26);
        listHash.put(listDataHeader.get(26),l27);
        listHash.put(listDataHeader.get(27),l28);









        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                if (groupPosition==0 && childPosition==0) {

                    Intent intent=new Intent(cs_paper.this, sensor.class);
                    startActivity(intent);
         //2--------------------------------------------------------------------------------------

                }
                if (groupPosition==1 && childPosition==0) {
                    Intent intent=new Intent(cs_paper.this, tc.class);
                    startActivity(intent);

                }
//3-----------------------------------------------------------------------------------------------
                if (groupPosition==2 && childPosition==0) {

                    Intent intent=new Intent(cs_paper.this, ds1.class);
                    startActivity(intent);

                }

                if (groupPosition==2 && childPosition==1) {

                    Intent intent=new Intent(cs_paper.this, ds2.class);
                    startActivity(intent);

                }

                if (groupPosition==2 && childPosition==2) {

                    Intent intent=new Intent(cs_paper.this, ds3.class);
                    startActivity(intent);

                }

                if (groupPosition==2 && childPosition==3) {
                    Intent intent=new Intent(cs_paper.this, ds4.class);
                    startActivity(intent);


                }


//4-----------------------------------------------------------------------------------------------------------------


                if (groupPosition==3 && childPosition==0) {

                    Intent intent=new Intent(cs_paper.this, coa1.class);
                    startActivity(intent);

                }

                if (groupPosition==3 && childPosition==1) {

                    Intent intent=new Intent(cs_paper.this, coa2.class);
                    startActivity(intent);

                }

                if (groupPosition==3 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, coa3.class);
                    startActivity(intent);


                }
//5----------------------------------------------------------------------------------------------------------

                if (groupPosition==4 && childPosition==0) {

                    Intent intent=new Intent(cs_paper.this, dstl1.class);
                    startActivity(intent);

                }

                if (groupPosition==4 && childPosition==1) {

                    Intent intent=new Intent(cs_paper.this, dstl2.class);
                    startActivity(intent);

                }

                if (groupPosition==4 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, dstl3.class);
                    startActivity(intent);


                }

//6------------------------------------------------------------------------------------------------------------
                if (groupPosition==5 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, math41.class);
                    startActivity(intent);


                }

//7----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==6 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, auto1.class);
                    startActivity(intent);


                }
                if (groupPosition==6 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, auto2.class);
                    startActivity(intent);


                }


                if (groupPosition==6 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, auto3.class);
                    startActivity(intent);


                }

//8--------------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==7 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, human1.class);
                    startActivity(intent);


                }

                if (groupPosition==7 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, human2.class);
                    startActivity(intent);


                }

                if (groupPosition==7 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, human3.class);
                    startActivity(intent);


                }
//9----------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==8 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, micro1.class);
                    startActivity(intent);


                }


                if (groupPosition==8 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, micro2.class);
                    startActivity(intent);


                }
                if (groupPosition==8 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, micro3.class);
                    startActivity(intent);


                }


//-------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==9 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, os1.class);
                    startActivity(intent);


                }


                if (groupPosition==9 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, os2.class);
                    startActivity(intent);


                }
                if (groupPosition==9 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, os3.class);
                    startActivity(intent);


                }

//------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==10 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, python.class);
                    startActivity(intent);


                }


//----------------------------------------------------------------------------------------------------------------

                if (groupPosition==11 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, eco1.class);
                    startActivity(intent);


                }



                if (groupPosition==11 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, eco2.class);
                    startActivity(intent);


                }

//------------------------------------------------------------------------------------------------------------------------------




                if (groupPosition==12 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, cyber1.class);
                    startActivity(intent);


                }



                if (groupPosition==12 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, cyber2.class);
                    startActivity(intent);

                }



//----------------------------------------------------------------------------------------------


                if (groupPosition==13 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, db1.class);
                    startActivity(intent);


                }



                if (groupPosition==13 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, db2.class);
                    startActivity(intent);

                }
                if (groupPosition==13 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, db3.class);
                    startActivity(intent);


                }



                if (groupPosition==13 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, db4.class);
                    startActivity(intent);

                }

//----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==14 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, daa1.class);
                    startActivity(intent);
                }
                if (groupPosition==14 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, daa2.class);
                    startActivity(intent);

                }
                if (groupPosition==14 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, daa3.class);
                    startActivity(intent);
                }
                if (groupPosition==14 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, daa4.class);
                    startActivity(intent);

                }

//-------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==15 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, pp1.class);
                    startActivity(intent);
                }
                if (groupPosition==15 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, pp2.class);
                    startActivity(intent);

                }
                if (groupPosition==15 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, ppl3.class);
                    startActivity(intent);
                }
                if (groupPosition==15 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, ppl4.class);
                    startActivity(intent);

                }

//---------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==16 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, wt1.class);
                    startActivity(intent);
                }
                if (groupPosition==16 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, wt2.class);
                    startActivity(intent);

                }
                if (groupPosition==16 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, wt3.class);
                    startActivity(intent);
                }
                if (groupPosition==16 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, wt4.class);
                    startActivity(intent);

                }



//-----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==17 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, im1.class);
                    startActivity(intent);
                }
                if (groupPosition==17 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, im2.class);
                    startActivity(intent);

                }
                if (groupPosition==17 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, im3.class);
                    startActivity(intent);
                }

//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==18 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, socio1.class);
                    startActivity(intent);
                }
                if (groupPosition==18 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, socio2.class);
                    startActivity(intent);

                }
 //------------------------------------------------------------------------------------------------------------


                if (groupPosition==19 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, cn1.class);
                    startActivity(intent);
                }
                if (groupPosition==19 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, cn2.class);
                    startActivity(intent);

                }

                if (groupPosition==19 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, cn3.class);
                    startActivity(intent);
                }

//---------------------------------------------------------------------------------------------------------------


                if (groupPosition==20 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, cd1.class);
                    startActivity(intent);
                }
                if (groupPosition==20 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, cd2.class);
                    startActivity(intent);

                }

                if (groupPosition==20 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, cd3.class);
                    startActivity(intent);
                }


   //---------------------------------------------------------------------------------------------------------
                if (groupPosition==21 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, cg1.class);
                    startActivity(intent);
                }

  //-----------------------------------------------------------------------------------------------------------
                   if (groupPosition==22 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, dw1.class);
                    startActivity(intent);
                }
                if (groupPosition==22 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, dw2.class);
                    startActivity(intent);

                }
                if (groupPosition==22 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, dw3.class);
                    startActivity(intent);

                }

//--------------------------------------------------------------------------------------------------------

                if (groupPosition==23 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, sc.class);
                    startActivity(intent);

                }

//---------------------------------------------------------------------------------------------------------
                if (groupPosition==24 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, cc.class);
                    startActivity(intent);

                }

///----------------------------------------------------------------------------------------------------------

                if (groupPosition==25 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, distri1.class);
                    startActivity(intent);
                }
                if (groupPosition==25 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, distri2.class);
                    startActivity(intent);

                }
                if (groupPosition==25 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, distri3.class);
                    startActivity(intent);

                }



                if (groupPosition==25 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, distri4.class);
                    startActivity(intent);
                }
       //--------------------------------------------------------------------------------------------------------
                if (groupPosition==26 && childPosition==0) {

                    Intent intent = new Intent(cs_paper.this, ai1.class);
                    startActivity(intent);

                }
                if (groupPosition==26 && childPosition==1) {

                    Intent intent = new Intent(cs_paper.this, ai2.class);
                    startActivity(intent);

                }
                if (groupPosition==26 && childPosition==2) {

                    Intent intent = new Intent(cs_paper.this, ai3.class);
                    startActivity(intent);

                }
                if (groupPosition==26 && childPosition==3) {

                    Intent intent = new Intent(cs_paper.this, ai4.class);
                    startActivity(intent);

                }

//----------------------------------------------------------------------------------------------------------------------





























                    return true;
            }
        });

    }


}
