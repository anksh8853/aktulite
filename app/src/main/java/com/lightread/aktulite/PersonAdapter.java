package com.lightread.aktulite;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.Viewholder> {


    private ArrayList<Person> people;
    ItemClicked activity;



    public interface ItemClicked {
        void onItemClicked(int index);
    }


    public PersonAdapter(ArrayList<Person> people, ItemClicked activity) {
        this.people = people;
        this.activity = activity;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(parent.getContext()).inflate(R.layout.aktu_service_list,parent,false);




        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.itemView.setTag(people.get(position));
        holder.pref.setImageResource(people.get(position).getPreference());
        holder.name.setText(people.get(position).getName());


    }

    @Override
    public int getItemCount() {
        return people.size();
    }






    public   class Viewholder extends RecyclerView.ViewHolder {
        ImageView pref;
        TextView name;

        public Viewholder(@NonNull final View itemView) {
            super(itemView);
            pref = itemView.findViewById(R.id.pref);
            name = itemView.findViewById(R.id.name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onItemClicked(people.indexOf((Person) v.getTag()));



                }


            });

        }
    }

}
