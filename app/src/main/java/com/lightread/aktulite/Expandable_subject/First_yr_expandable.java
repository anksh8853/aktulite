package com.lightread.aktulite.Expandable_subject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdSize;
import com.lightread.aktulite.Expandable_subject.che.che1;
import com.lightread.aktulite.Expandable_subject.che.che2;
import com.lightread.aktulite.Expandable_subject.che.che3;
import com.lightread.aktulite.Expandable_subject.che.che4;
import com.lightread.aktulite.Expandable_subject.ee.ee1;
import com.lightread.aktulite.Expandable_subject.ee.ee2;
import com.lightread.aktulite.Expandable_subject.ee.ee3;
import com.lightread.aktulite.Expandable_subject.ee.ee4;
import com.lightread.aktulite.Expandable_subject.math.math1;
import com.lightread.aktulite.Expandable_subject.math.math2;
import com.lightread.aktulite.Expandable_subject.math.math3;
import com.lightread.aktulite.Expandable_subject.math.math4;
import com.lightread.aktulite.Expandable_subject.math2.math22;
import com.lightread.aktulite.Expandable_subject.math2.math23;
import com.lightread.aktulite.Expandable_subject.math2.math24;
import com.lightread.aktulite.Expandable_subject.phy.phy1;
import com.lightread.aktulite.Expandable_subject.phy.phy2;
import com.lightread.aktulite.Expandable_subject.pp.pp1;
import com.lightread.aktulite.Expandable_subject.pp.pp2;
import com.lightread.aktulite.ExpandablelistAdapter;
import com.lightread.aktulite.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class First_yr_expandable extends AppCompatActivity {

    private ExpandableListView listView;
    private ExpandablelistAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;
    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_yr_expandable);







        listView=findViewById(R.id.exp);


        initData();
        listAdapter=new ExpandablelistAdapter(this,listDataHeader,listHash);

        listView.setAdapter(listAdapter);

    }


    private void initData() {

        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();

        listDataHeader.add("Physics");
        listDataHeader.add("Mathematics I");
        listDataHeader.add("Basic Electrical Engineering");
        listDataHeader.add("Chemistry");
        listDataHeader.add("Mathematics II");
        listDataHeader.add("Programming for Problem Solving");
        listDataHeader.add("Professional English");





        List<String> phy=new ArrayList<>();
        phy.add("1.   Question Paper (2019-2020)");
        phy.add("2.   Question Paper (2018-2019)");




        List<String> math=new ArrayList<>();
        math.add("1.   Question Paper (2019-2020)");
        math.add("2.   Question Paper (2018-2019)");
        math.add("3.   Question Paper (2017-2018)");
        math.add("4.   Question Paper (2016-2017)");


        List<String> ele=new ArrayList<>();
        ele.add("1.   Question Paper (2019-2020)");
        ele.add("2.   Question Paper (2018-2019)");
        ele.add("3.   Question Paper (2017-2018)");
        ele.add("4.   Question Paper (2016-2017)");



        List<String> che=new ArrayList<>();
        che.add("1.   Question Paper (2019-2020)");
        che.add("2.   Question Paper (2018-2019)");
        che.add("3.   Question Paper (2017-2018)");
        che.add("4.   Question Paper (2016-2017)");



        List<String> m2=new ArrayList<>();

        m2.add("1.   Question Paper (2018-2019)");
        m2.add("2.   Question Paper (2017-2018)");
        m2.add("3.   Question Paper (2016-2017)");


        List<String> pp=new ArrayList<>();
        pp.add("1.   Question Paper (2019-2020)");
        pp.add("2.   Question Paper (2018-2019)");


        List<String> pe=new ArrayList<>();
        pe.add("1.   Question Paper (2018-2019");


        listHash.put(listDataHeader.get(0),phy);
        listHash.put(listDataHeader.get(1),math);
        listHash.put(listDataHeader.get(2),ele);
        listHash.put(listDataHeader.get(3),che);
        listHash.put(listDataHeader.get(4),m2);
        listHash.put(listDataHeader.get(5),pp);
        listHash.put(listDataHeader.get(6),pe);



        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {



       //1-------------------------------------------------------------------------------------------------
                if (groupPosition==0 && childPosition==0) {
                    Intent intent=new Intent(First_yr_expandable.this, phy1.class);
                    startActivity(intent);


                }
                if (groupPosition==0 && childPosition==1) {
                    Intent intent=new Intent(First_yr_expandable.this, phy2.class);
                    startActivity(intent);


                }

//2-------------------------------------------------------------------------------------------------------------
                if (groupPosition==1 && childPosition==0) {
                    Intent intent=new Intent(First_yr_expandable.this, math1.class);
                    startActivity(intent);


                }
                if (groupPosition==1 && childPosition==1)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math2.class);
                    startActivity(intent);
                }
                if (groupPosition==1 && childPosition==2)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math3.class);
                    startActivity(intent);
                }
                if (groupPosition==1 && childPosition==3)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math4.class);
                    startActivity(intent);
                }
//3--------------------------------------------------------------------------------------------------------------------
                if (groupPosition==2 && childPosition==0)
                {
                    Intent intent=new Intent(First_yr_expandable.this, ee1.class);
                    startActivity(intent);

                }

                if (groupPosition==2 && childPosition==1)
                {
                    Intent intent=new Intent(First_yr_expandable.this, ee2.class);
                    startActivity(intent);
                }

                if (groupPosition==2 && childPosition==2)
                {
                    Intent intent=new Intent(First_yr_expandable.this, ee3.class);
                    startActivity(intent);
                }

                if (groupPosition==2 && childPosition==3)
                {
                    Intent intent=new Intent(First_yr_expandable.this, ee4.class);
                    startActivity(intent);
                }

//4------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==3 && childPosition==0)
                {
                    Intent intent=new Intent(First_yr_expandable.this, che1.class);
                    startActivity(intent);

                }

                if (groupPosition==3 && childPosition==1)
                {
                    Intent intent=new Intent(First_yr_expandable.this, che2.class);
                    startActivity(intent);
                }

                if (groupPosition==3 && childPosition==2)
                {
                    Intent intent=new Intent(First_yr_expandable.this, che3.class);
                    startActivity(intent);
                }

                if (groupPosition==3 && childPosition==3)
                {
                    Intent intent=new Intent(First_yr_expandable.this, che4.class);
                    startActivity(intent);
                }

                //5-------------------------------------------------------------------------------------------
                if (groupPosition==4 && childPosition==0)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math22.class);
                    startActivity(intent);

                }

                if (groupPosition==4 && childPosition==1)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math23.class);
                    startActivity(intent);
                }

                if (groupPosition==4 && childPosition==2)
                {
                    Intent intent=new Intent(First_yr_expandable.this, math24.class);
                    startActivity(intent);
                }
//6--------------------------------------------------------------------------------------------------------------

                if (groupPosition==5 && childPosition==0)
                {
                    Intent intent=new Intent(First_yr_expandable.this, pp1.class);
                    startActivity(intent);

                }

                if (groupPosition==5 && childPosition==1)
                {
                    Intent intent=new Intent(First_yr_expandable.this, pp2.class);
                    startActivity(intent);
                }

//7--------------------------------------------------------------------------------------------------------------
                if (groupPosition==6 && childPosition==0)
                {
                    Intent intent=new Intent(First_yr_expandable.this, Prof_Eng.class);
                    startActivity(intent);

                }














                return true;
            }
        });
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


}
