package com.lightread.aktulite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent I =new Intent(Splash_Screen.this,MainActivity.class);
                                    startActivity(I);
                                    finish();


                                }
                            },


                2000);

    }
    }

