package com.lightread.aktulite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;



public class AktuService extends AppCompatActivity implements PersonAdapter.ItemClicked {


    RecyclerView recyclerView;
    RecyclerView.Adapter myadapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Person> people;
    private AdView mAdView;



    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aktu_service);




        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        recyclerView=findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        // layoutManager=new LinearLayoutManager(this);

        layoutManager=new GridLayoutManager(this,2, GridLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        people=new ArrayList<Person>();
        people.add(new Person("Result",R.drawable.res));
        people.add(new Person("Dashboard",R.drawable.dash));

        people.add(new Person("ERP Login",R.drawable.erp));
        people.add(new Person("Syllabus",R.drawable.syll));



        myadapter=new PersonAdapter(people,this);
        recyclerView.setAdapter(myadapter);
    }





    @Override
    public void onItemClicked(int index) {


        final Intent intent;


        switch (index)

        {
            case 0:


                intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://erp.aktu.ac.in/webpages/oneview/oneview.aspx"));
                startActivity(intent);
                break;

            case 1:

                intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://aktu.ac.in"));
                startActivity(intent);
                break;

            case 2:
                intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://erp.aktu.ac.in/Login.aspx"));
                startActivity(intent);
                break;

            case 3:
                intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://aktu.ac.in/syllabus.html"));
                startActivity(intent);
                break;


        }


    }


    }

