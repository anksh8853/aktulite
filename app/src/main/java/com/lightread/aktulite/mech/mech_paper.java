package com.lightread.aktulite.mech;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.lightread.aktulite.ExpandablelistAdapter;
import com.lightread.aktulite.R;
import com.lightread.aktulite.civil.civil_paper;
import com.lightread.aktulite.cs.cyber.cyber1;
import com.lightread.aktulite.cs.cyber.cyber2;
import com.lightread.aktulite.cs.eco.eco1;
import com.lightread.aktulite.cs.eco.eco2;
import com.lightread.aktulite.cs.human.human1;
import com.lightread.aktulite.cs.human.human2;
import com.lightread.aktulite.cs.human.human3;
import com.lightread.aktulite.cs.im.im1;
import com.lightread.aktulite.cs.im.im2;
import com.lightread.aktulite.cs.im.im3;
import com.lightread.aktulite.cs.math4.math41;
import com.lightread.aktulite.cs.python.python;
import com.lightread.aktulite.cs.socio.socio1;
import com.lightread.aktulite.cs.socio.socio2;
import com.lightread.aktulite.cs.tc.tc;
import com.lightread.aktulite.mech.appliedthermo.ap1;
import com.lightread.aktulite.mech.appliedthermo.ap2;
import com.lightread.aktulite.mech.appliedthermo.ap3;
import com.lightread.aktulite.mech.automobile.autom1;
import com.lightread.aktulite.mech.automobile.autom2;
import com.lightread.aktulite.mech.automobile.autom3;
import com.lightread.aktulite.mech.automobile.autom4;
import com.lightread.aktulite.mech.cadcam.cadcam1;
import com.lightread.aktulite.mech.enggmech.em;
import com.lightread.aktulite.mech.fluidmach.fm1;
import com.lightread.aktulite.mech.fluidmach.fm2;
import com.lightread.aktulite.mech.fluidmach.fm3;
import com.lightread.aktulite.mech.fluidmm.fluidmm;
import com.lightread.aktulite.mech.heatnmass.hm1;
import com.lightread.aktulite.mech.heatnmass.hm2;
import com.lightread.aktulite.mech.heatnmass.hm3;
import com.lightread.aktulite.mech.heatnmass.hm4;
import com.lightread.aktulite.mech.ic.ic1;
import com.lightread.aktulite.mech.ic.ic2;
import com.lightread.aktulite.mech.ic.ic3;
import com.lightread.aktulite.mech.ic.ic4;
import com.lightread.aktulite.mech.machine_d1.m1;
import com.lightread.aktulite.mech.machine_d1.m2;
import com.lightread.aktulite.mech.machine_d1.m3;
import com.lightread.aktulite.mech.machine_d1.m4;
import com.lightread.aktulite.mech.machinedesign2.md21;
import com.lightread.aktulite.mech.machinedesign2.md22;
import com.lightread.aktulite.mech.manuscintech.ms1;
import com.lightread.aktulite.mech.manuscintech.ms2;
import com.lightread.aktulite.mech.manuscintech.ms3;
import com.lightread.aktulite.mech.manuscintech.ms4;
import com.lightread.aktulite.mech.materialengg.mae;
import com.lightread.aktulite.mech.operationresearch.op1;
import com.lightread.aktulite.mech.operationresearch.op2;
import com.lightread.aktulite.mech.powerplant.p1;
import com.lightread.aktulite.mech.refrigerator.r1;
import com.lightread.aktulite.mech.refrigerator.r2;
import com.lightread.aktulite.mech.refrigerator.r3;
import com.lightread.aktulite.mech.theofmachine.tom1;
import com.lightread.aktulite.mech.thermo.thermo1;
import com.lightread.aktulite.mech.thermo.thermo2;
import com.lightread.aktulite.mech.thermo.thermo3;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class mech_paper extends AppCompatActivity {
    private ExpandableListView listView;
    private ExpandablelistAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mech_paper);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);



        listView=findViewById(R.id.exp);
        initData();
        listAdapter=new ExpandablelistAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);

    }

    private void initData() {

        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();

        //-----------------------------------------------------
        listDataHeader.add("Mathematics-IV");
        listDataHeader.add("Universal Human Values & Professional Ethics");
        listDataHeader.add("Thermodynamics");
        listDataHeader.add("Fluid Mechanics & Fluid Machines");
        listDataHeader.add("Materials Engineering");

        listDataHeader.add("Applied Thermodynamics");
        listDataHeader.add("Engineering Mechanics");
        listDataHeader.add("Manufacturing Processes");
        listDataHeader.add("Technical Communication");
        listDataHeader.add("Engg. Science Course");
        listDataHeader.add("Python Programming");
        //------------------------------------------------------
        listDataHeader.add("Managerial Economics");
        listDataHeader.add("Sociology");

        listDataHeader.add("Machine Design-I");
        listDataHeader.add("Heat & Mass Transfer");
        listDataHeader.add("Manufacturing Science& Technology-II");
        listDataHeader.add("IC Engines and Compressors");
        listDataHeader.add("Industrial Managment");
        listDataHeader.add("Cyber Security");

        listDataHeader.add("Fluid Machinery");
        listDataHeader.add("Theory of Machines");
        listDataHeader.add("Machine Design-II");
        listDataHeader.add("Refrigeration & Air-conditioning");
        //------------------------------------------------------

        listDataHeader.add("Power Plant Engineering");
        listDataHeader.add("Operation Research");
        listDataHeader.add("CAD/CAM");
        listDataHeader.add("Automobile Engineering");
        listDataHeader.add("");













        List<String> l1=new ArrayList<>();
        l1.add("1.   Question Paper (2019-2020)");





        List<String> l2=new ArrayList<>();
        l2.add("1.   Question Paper (2018-2019)");
        l2.add("2.   Question Paper (2017-2018)");
        l2.add("3.   Question Paper (2016-2017)");


        List<String> l3=new ArrayList<>();
        l3.add("1.   Question Paper (2019-2020)");
        l3.add("2.   Question Paper (2018-2019)");
        l3.add("3.   Question Paper (2017-2018)");




        List<String> l4=new ArrayList<>();
        l4.add("1.   Question Paper (2019-2020)");




        List<String> l5=new ArrayList<>();
        l5.add("1.   Question Paper (2019-2020)");



        List<String> l6=new ArrayList<>();
        l6.add("1.   Question Paper (2018-2019)");
        l6.add("2.   Question Paper (2017-2018)");
        l6.add("3.   Question Paper (2016-2017)");


        List<String> l7=new ArrayList<>();
        l7.add("1.   Question Paper (2019-2020)");



        List<String> l8=new ArrayList<>();
        l8.add("Prevoius Year Paper Not Found");



        List<String> l9=new ArrayList<>();
        l9.add("1.   Question Paper (2019-2020)");



        List<String> l10=new ArrayList<>();
        l10.add("Prevoius Year Paper Not Found");


        List<String> l11=new ArrayList<>();
        l11.add("1.   Question Paper (2019-2020)");
//----------------------------------------------------------------------
        List<String> l12=new ArrayList<>();
        l12.add("1.   Question Paper (2019-2020)");
        l12.add("2.   Question Paper (2018-2019)");


        List<String> l13=new ArrayList<>();
        l13.add("1.   Question Paper (2019-2020)");
        l13.add("2.   Question Paper (2018-2019)");


        List<String> l14=new ArrayList<>();
        l14.add("1.   Question Paper (2019-2020)");
        l14.add("2.   Question Paper (2018-2019)");
        l14.add("3.   Question Paper (2017-2018)");
        l14.add("4.   Question Paper (2016-2017)");

        List<String> l15=new ArrayList<>();
        l15.add("1.   Question Paper (2019-2020)");
        l15.add("2.   Question Paper (2018-2019)");
        l15.add("3.   Question Paper (2017-2018)");
        l15.add("4.   Question Paper (2016-2017)");


        List<String> l16=new ArrayList<>();
        l16.add("1.   Question Paper (2019-2020)");
        l16.add("2.   Question Paper (2018-2019)");
        l16.add("3.   Question Paper (2017-2018)");
        l16.add("4.   Question Paper (2016-2017)");

        List<String> l17=new ArrayList<>();
        l17.add("1.   Question Paper (2019-2020)");
        l17.add("2.   Question Paper (2018-2019)");
        l17.add("3.   Question Paper (2017-2018)");
        l17.add("4.   Question Paper (2016-2017)");

        List<String> l18=new ArrayList<>();

        l18.add("1.   Question Paper (2018-2019)");
        l18.add("2.   Question Paper (2017-2018)");
        l18.add("3.   Question Paper (2016-2017)");

        List<String> l19=new ArrayList<>();
        l19.add("1.   Question Paper (2019-2020)");
        l19.add("2.   Question Paper (2018-2019)");



        List<String> l20=new ArrayList<>();

        l20.add("1.   Question Paper (2018-2019)");
        l20.add("2.   Question Paper (2017-2018)");
        l20.add("3.   Question Paper (2016-2017)");

        List<String> l21=new ArrayList<>();

        l21.add("1.   Question Paper (2018-2019)");


        List<String> l22=new ArrayList<>();

        l22.add("1.   Question Paper (2018-2019)");
        l22.add("2.   Question Paper (2017-2018)");



        List<String> l23=new ArrayList<>();

        l23.add("1.   Question Paper (2018-2019)");
        l23.add("2.   Question Paper (2017-2018)");
        l23.add("3.   Question Paper (2016-2017)");


        //------------------------------------------------




        List<String> l24=new ArrayList<>();
        l24.add("1.   Question Paper (2019-2020)");


        List<String> l25=new ArrayList<>();
        l25.add("1.   Question Paper (2019-2020)");
        l25.add("2.   Question Paper (2018-2019)");


        List<String> l26=new ArrayList<>();
        l26.add("1.   Question Paper (2019-2020)");


        List<String> l27=new ArrayList<>();
        l27.add("1.   Question Paper (2019-2020)");
        l27.add("2.   Question Paper (2018-2019)");
        l27.add("3.   Question Paper (2017-2018)");
        l27.add("4.   Question Paper (2016-2017)");

        List<String> l28=new ArrayList<>();




        listHash.put(listDataHeader.get(0),l1);
        listHash.put(listDataHeader.get(1),l2);
        listHash.put(listDataHeader.get(2),l3);
        listHash.put(listDataHeader.get(3),l4);
        listHash.put(listDataHeader.get(4),l5);
        listHash.put(listDataHeader.get(5),l6);
        listHash.put(listDataHeader.get(6),l7);
        listHash.put(listDataHeader.get(7),l8);
        listHash.put(listDataHeader.get(8),l9);
        listHash.put(listDataHeader.get(9),l10);
        listHash.put(listDataHeader.get(10),l11);
        listHash.put(listDataHeader.get(11),l12);
        listHash.put(listDataHeader.get(12),l13);
        listHash.put(listDataHeader.get(13),l14);
        listHash.put(listDataHeader.get(14),l15);
        listHash.put(listDataHeader.get(15),l16);
        listHash.put(listDataHeader.get(16),l17);
        listHash.put(listDataHeader.get(17),l18);
        listHash.put(listDataHeader.get(18),l19);
        listHash.put(listDataHeader.get(19),l20);
        listHash.put(listDataHeader.get(20),l21);
        listHash.put(listDataHeader.get(21),l22);
        listHash.put(listDataHeader.get(22),l23);
        listHash.put(listDataHeader.get(23),l24);
        listHash.put(listDataHeader.get(24),l25);
        listHash.put(listDataHeader.get(25),l26);
        listHash.put(listDataHeader.get(26),l27);
        listHash.put(listDataHeader.get(27),l28);



        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

//-----------------------------------------------------------------------------------------------------------------
                if (groupPosition==0 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, math41.class);
                    startActivity(intent);

                }

//---------------------------------------------------------------------------------------------------------------------
                if (groupPosition==1 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, human1.class);
                    startActivity(intent);
                }
                if (groupPosition==1 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, human2.class);
                    startActivity(intent);
                }


                if (groupPosition==1 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, human3.class);
                    startActivity(intent);
                }

//---------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==2 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, thermo1.class);
                    startActivity(intent);
                }
                if (groupPosition==2 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, thermo2.class);
                    startActivity(intent);
                }


                if (groupPosition==2 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, thermo3.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==3 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, fluidmm.class);
                    startActivity(intent);
                }

//--------------------------------------------------------------------------------------------------------------

                if (groupPosition==4 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, mae.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------------------------


                if (groupPosition==5 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, ap1.class);
                    startActivity(intent);
                }

                if (groupPosition==5 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, ap2.class);
                    startActivity(intent);
                }

                if (groupPosition==5 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, ap3.class);
                    startActivity(intent);
                }
//--------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==6 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, em.class);
                    startActivity(intent);
                }


//===========================================================================================================
                if (groupPosition==7 && childPosition==0)
                {
                    Toast.makeText(mech_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();
                }

//-----------------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==8 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, tc.class);
                    startActivity(intent);
                }

//----------------------------------------------------------------------------------------------------------------------
                if (groupPosition==9 && childPosition==0)
                {
                    Toast.makeText(mech_paper.this, "If You Have,Mail at:-anksh8853@gmail.com", Toast.LENGTH_LONG).show();
                }

//---------------------------------------------------------------------------------------------------------------------------



                if (groupPosition==10 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, python.class);
                    startActivity(intent);
                }

//--------------------------------------------------------------------------------------------------------

                if (groupPosition==11 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, eco1.class);
                    startActivity(intent);
                }


                if (groupPosition==11 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, eco2.class);
                    startActivity(intent);
                }

 //---------------------------------------------------------------------------------------------------------------------
                if (groupPosition==12 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, socio1.class);
                    startActivity(intent);
                }


                if (groupPosition==12 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, socio2.class);
                    startActivity(intent);
                }


//-----------------------------------------------------------------------------------------------------
                if (groupPosition==13 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, m1.class);
                    startActivity(intent);
                }


                if (groupPosition==13 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, m2.class);
                    startActivity(intent);
                }

                if (groupPosition==13 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, m3.class);
                    startActivity(intent);
                }


                if (groupPosition==13 && childPosition==3)
                {
                    Intent intent=new Intent(mech_paper.this, m4.class);
                    startActivity(intent);
                }
//----------------------------------------------------------------------------------------------------------------------

                if (groupPosition==14 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, hm1.class);
                    startActivity(intent);
                }


                if (groupPosition==14 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, hm2.class);
                    startActivity(intent);
                }

                if (groupPosition==14 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, hm3.class);
                    startActivity(intent);
                }


                if (groupPosition==14 && childPosition==3)
                {
                    Intent intent=new Intent(mech_paper.this, hm4.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------
                if (groupPosition==15 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, ms1.class);
                    startActivity(intent);
                }


                if (groupPosition==15 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, ms2.class);
                    startActivity(intent);
                }

                if (groupPosition==15 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, ms3.class);
                    startActivity(intent);
                }


                if (groupPosition==15 && childPosition==3)
                {
                    Intent intent=new Intent(mech_paper.this, ms4.class);
                    startActivity(intent);
                }

//-----------------------------------------------------------------------------------


                if (groupPosition==16 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, ic1.class);
                    startActivity(intent);
                }


                if (groupPosition==16 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, ic2.class);
                    startActivity(intent);
                }

                if (groupPosition==16 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, ic3.class);
                    startActivity(intent);
                }


                if (groupPosition==16 && childPosition==3)
                {
                    Intent intent=new Intent(mech_paper.this, ic4.class);
                    startActivity(intent);
                }


//-----------------------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==17 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, im1.class);
                    startActivity(intent);
                }


                if (groupPosition==17 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, im2.class);
                    startActivity(intent);
                }

                if (groupPosition==17 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, im3.class);
                    startActivity(intent);
                }


//------------------------------------------------------------------------------------------------------------------------


                if (groupPosition==18 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, cyber1.class);
                    startActivity(intent);
                }


                if (groupPosition==18 && childPosition==1) {
                    Intent intent = new Intent(mech_paper.this, cyber2.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==19 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, fm1.class);
                    startActivity(intent);
                }


                if (groupPosition==19 && childPosition==1) {
                    Intent intent = new Intent(mech_paper.this, fm2.class);
                    startActivity(intent);
                }

                if (groupPosition==19 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, fm3.class);
                    startActivity(intent);
                }
//----------------------------------------------------------------------------------------------------------------------------

                if (groupPosition==20 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, tom1.class);
                    startActivity(intent);
                }

//-----------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==21 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, md21.class);
                    startActivity(intent);
                }

                if (groupPosition==21 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, md22.class);
                    startActivity(intent);
                }

//--------------------------------------------------------------------------------------------------------------------
                if (groupPosition==22 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, r1.class);
                    startActivity(intent);
                }

                if (groupPosition==22 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, r2.class);
                    startActivity(intent);
                }
                if (groupPosition==22 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, r3.class);
                    startActivity(intent);
                }
//--------------------------------------------------------------------------------------------------------------------------
                if (groupPosition==23 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, p1.class);
                    startActivity(intent);
                }
//---------------------------------------------------------------------------------

                if (groupPosition==24 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, op1.class);
                    startActivity(intent);
                }
                if (groupPosition==24 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, op2.class);
                    startActivity(intent);
                }
//-----------------------------------------------------------------------------------------------------------

                if (groupPosition==25 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, cadcam1.class);
                    startActivity(intent);
                }
//-----------------------------------------------------------------------------------------------------------------
                if (groupPosition==26 && childPosition==0)
                {
                    Intent intent=new Intent(mech_paper.this, autom1.class);
                    startActivity(intent);
                }
                if (groupPosition==26 && childPosition==1)
                {
                    Intent intent=new Intent(mech_paper.this, autom2.class);
                    startActivity(intent);
                }

                if (groupPosition==26 && childPosition==2)
                {
                    Intent intent=new Intent(mech_paper.this, autom3.class);
                    startActivity(intent);
                }
                if (groupPosition==26 && childPosition==3)
                {
                    Intent intent=new Intent(mech_paper.this, autom4.class);
                    startActivity(intent);
                }

//-------------------------------------------------------------------------------------------------------









                return true;
            }
        });

    }


}
