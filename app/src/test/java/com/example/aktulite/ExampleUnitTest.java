package com.lightread.aktulite;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * lightread local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class lightreadUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}